# My Requests in JIRA Service Desk
This add-on demonstrates simple usage of Atlassian Connect with JIRA Service Desk, including the newly released REST 
API.

See the [My Requests tutorial](https://developer.atlassian.com/static/connect/docs/latest/guides/my-requests-tutorial.html) 
in the Atlassian Connect documentation for instructions on getting set up.